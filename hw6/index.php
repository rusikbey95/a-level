<?php
session_start();
echo '<pre>';
print_r($_SESSION);
echo '</pre>';

$_SESSION['travelCountries'] = $_REQUEST['travelCountries'];
$_SESSION['travelDays'] = $_REQUEST['travelDays'];
$_SESSION['travelSale'] = $_REQUEST['travelSale'];
$_SESSION['registerfirstName'] = $_REQUEST['registerfirstName'];
$_SESSION['registerPassword'] = $_REQUEST['registerPassword'];
$_SESSION['registerEmail'] = $_REQUEST['registerEmail'];
$_SESSION['randomNumber'] = $_REQUEST['randomNumber'];

// *1
echo '*1' . ' <br>';
/**
 * *1
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @return array
 */
function getRandomNumbers(): array
{
    $i = 0;
    $arr = [];
    while ($i <= 100) {
        $randomNumber = rand(1, 100);
        $arr[$i] = $randomNumber;
        ++$i;
    }
    return $arr;
}

/**
 * 1
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @return array
 */
function countEvenAndAboveZeroNambers(array $arr): array
{
    $arr2 = [];
    foreach ($arr as $key => $value) {
        if ($value > 0 && $key % 2 == 0) {
            $arr2[] = $value * $value;
        }
    }
    return $arr2;
}

/**
 * 1
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @return array
 */
function showOddAndAboveZeroNambers(array $arr): array
{
    $arr2 = [];
    foreach ($arr as $key => $value) {
        if ($value > 0 && $key % 2 !== 0) {
            $arr2[$key] = $value;
        }
    }
    return $arr2;
}
echo '<pre>';
print_r(showOddAndAboveZeroNambers(countEvenAndAboveZeroNambers(getRandomNumbers())));
echo '<pre>';
echo '<br><br>';

// *2
echo '*2' . ' <br>';

/**
 * 2
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @return array
 */
function countSumNumbers(int $a, int $b): array // phpcs:ignore
{
    $arr = [];
    $arr[] = $a + $b;
    $arr[] = $a * $b;
    return $arr;
}

/**
 * 2
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @return array
 */
function countSumSquares(int $a, int $b): array // phpcs:ignore
{
    $arr = [];
    $arr[] = $a * $a + $b * $b;
    return $arr;
}
echo '<pre>';
print_r(countSumNumbers(10, 2));
print_r(countSumSquares(10, 2));
echo '<pre>';
echo '<br><br>';

// *3
echo '*3' . ' <br>';

/**
 * 3
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return float
 */
function countAverageNumbers(int $a, int $b, int $c): float // phpcs:ignore
{
    $result = ($a + $b + $c) / 3;
    return $result;
}
echo '<pre>';
print_r(countAverageNumbers(32, 2, 2));
echo '<pre>';
echo '<br><br>';

// *4
echo '*4' . ' <br>';

/**
 * 4
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @return integer
 */
function countPercentOnThirty(int $a): int
{
    $percentResult = $a * 30 / 100;
    return $a + $percentResult;
}
echo countPercentOnThirty(100) . '<br>';

/**
 * 4
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @return integer
 */
function countPercentOnOneHundredTwenty(int $a): int
{
    $percentResult = $a * 120 / 100;
    return $a + $percentResult;
}
echo countPercentOnOneHundredTwenty(100);
echo '<br><br>';

// *5
echo '*5' . ' <br>';
?>

    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
        <select name="travelCountries" required>
            <option value="Турция" name="tr">Турция</option>
            <option value="Египет" name="eg">Египет</option>
            <option value="Италия" name="it">Италия</option>
        </select>
        <input type="text" name="travelDays" placeholder="Кол-во дней" required>
        <input type="checkbox" name="travelSale" value="Скидка">
        <button type="submit">Отправить</button>
    </form>

<?php

/**
 * 5
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @return integer
 */
function countPercentPlus(int $a, int $b): int
{
    $percentResult = $a * $b / 100;
    return $a + $percentResult;
}

/**
 * 5
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @return integer
 */
function countPercentMinus(int $a, int $b): int
{
    $percentResult = $a * $b / 100;
    return $a - $percentResult;
}

/**
 * 5
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param string $country
 * @param integer $day
 * @param integer $sale
 * @return integer
 */
function countTravelPrice(string $country, int $day, int $sale): int
{
    $days = $day * 400;
    if ($country == 'Египет') {
        $countResult = countPercentPlus($days, 10);
    } elseif ($country == 'Италия') {
        $countResult = countPercentPlus($days, 12);
    }

    return $sale ? countPercentMinus($countResult, 5) : $countResult;
}
if ($_SESSION['travelCountries']) {
    echo countTravelPrice($_SESSION['travelCountries'], $_SESSION['travelDays'], $_SESSION['travelSale']);
}
echo '<br><br>';

// *6
echo '*6' . ' <br>';
?>

    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
        <input type="text" name="registerfirstName" placeholder="Имя">
        <input type="email" name="registerEmail" placeholder="Email">
        <input type="password" name="registerPassword" placeholder="Password">
        <button type="submit">Отправить</button>
    </form>

<?php
/**
 * 6
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param string $name
 * @param string $email
 * @param string $password
 * @return string
 */
function checkRegisterForm(string $name, string $email, string $password): string
{
    if ($name && $email && $password) {
        return 'Регистрация прошла успешно' . print_r($_POST);
    } elseif (empty($name)) {
        return 'Заполните поле имя';
    } elseif (empty($email)) {
        return 'Заполните поле email';
    } elseif (empty($password)) {
        return 'Заполните поле password';
    }

    return '';
}
echo '<br><br>';

// *7
echo '*7' . ' <br>';
?>

    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
        <input type="text" name="randomNumber" placeholder="Введите число">
        <button type="submit">Отправить</button>
    </form>

<?php
/**
 * 7
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @return array|string
 */
function showText(int $a): array
{
    $text = [];
    if (is_int($a) && $a > 0) {
        for ($i = 1; $i <= $a; ++$i) {
            $text[] = 'Silence is golden';
        }
    } else {
        return 'Bad n';
    }

    return $text;
}
if ($_SESSION['randomNumber']) {
    print_r(showText((int) $_SESSION['randomNumber']));
} else {
    echo 'Bad n';
}
echo '<br><br>';

// *8
echo '*8' . ' <br>';

/**
 * 8
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param float $a
 * @return array
 */
function getBinary(float $a): array
{
    $arr = [];
    for ($i = 0; $i <= $a; $i++) {
        if ($i % 2 == 0) {
            $arr[] = 0;
        } else {
            $arr[] = 1;
        }
    }

    return $arr;
}
print_r(getBinary(10));
echo '<br><br>';

// *9
echo '*9' . ' <br>';
$array = [23, 2, 2, 3, 45, 3, 56];
$arrResult = [];
/**
 * 9
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @return array
 */
function checkRepeatNumbers(array $arr): array
{
    foreach ($arr as $key => $value) {
        if (isset($arrResult[$value])) {
            $arrResult[$value] += 1;
        } else {
            $arrResult[$value] = 1;
        }
    }
    return $arrResult;
}
print_r(checkRepeatNumbers($array));
echo '<br><br>';

// *10
echo '*10' . ' <br>';

/**
 * 10
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return integer
 */
function checkMinNumber(int $a, int $b, int $c) : int
{
    if ($a < $b && $a < $c) {
        return $a;
    } elseif ($b < $a && $b < $c) {
        return $b;
    } else {
        return $c;
    }
}
echo checkMinNumber(10, 52, 14);
echo '<br>';

/**
 * 10
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return integer
 */
function checkMaxNumber(int $a, int $b, int $c): int
{
    if ($a > $b && $a > $c) {
        return $a;
    } elseif ($b > $a && $b > $c) {
        return $b;
    } else {
        return $c;
    }
}
echo checkMaxNumber(10, 52, 14);
echo '<br><br>';

// *11
echo '*11' . ' <br>';

/**
 * 11
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @return integer
 */
function calcSquare(int $a, int $b): int
{
    return $a * $b;
}
echo calcSquare(10, 10);
echo '<br><br>';

// *12
echo '*12' . ' <br>';

/**
 * 12
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @return float
 */
function calcPythagoreanTheorem(int $a, int $b): float
{
    $c = $a * $a + $b * $b;
    return sqrt($c);
}
echo calcPythagoreanTheorem(2, 4);
echo '<br><br>';

// *13
echo '*13' . ' <br>';

/**
 * 13
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @return integer
 */
function calcPerimeter(int $a, int $b): int
{
    return 2 * ($a + $b);
}
echo calcPerimeter(2, 4);
echo '<br><br>';

// *14
echo '*14' . ' <br>';
/**
 * 14
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return float
 */
function calcDiscriminant(int $a, int $b, int $c): float
{
    return $b * $b - 4 * $a * $c;
}
echo calcDiscriminant(4, 3, 2);
echo '<br><br>';

// *15
echo '*15' . ' <br>';

/**
 * 15
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @return array
 */
function getEvenNumbers(int $a): array
{
    $arr = [];
    for ($i = 1; $i <= $a; $i++) {
        if ($i % 2 == 0) {
            $arr[] = $i;
        }
    }
    return $arr;
}
print_r(getEvenNumbers(100));
echo '<br><br>';

// *16
echo '*16' . ' <br>';

/**
 * 16
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @return array
 */
function getOddNumbers(int $a): array
{
    $arr = [];
    for ($i = 1; $i <= $a; $i++) {
        if ($i % 2 !== 0) {
            $arr[] = $i;
        }
    }
    return $arr;
}
print_r(getOddNumbers(100));
echo '<br><br>';

// **1
echo '**1' . ' <br>';

/**
 * 1
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param integer $a
 * @return integer
 */
function calcPowerNumber(int $a): int
{
    return $a * $a;
}
echo calcPowerNumber(10);
echo '<br><br>';

// **2
echo '**2' . ' <br>';

/**
 * 2
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @return integer
 */
function countArr(array $arr): int
{
    $count = 0;
    foreach ($arr as $value) {
        $count++;
    }
    return $count;
}

/**
 * 2
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @param string $order
 * @return array
 */
function sortArr(array $arr, string $order = 'asc'): array
{
    $count = countArr($arr);
    if ($order == 'asc') {
        for ($i = 1; $i < $count; $i++) {
            $cur_val = $arr[$i];
            $j = $i - 1;

            while (isset($arr[$j]) && $arr[$j] > $cur_val) {
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $cur_val;
                $j--;
            }
        }
        return $arr;
    } elseif ($order == 'desc') {
        for ($i = 1; $i < $count; $i++) {
            $cur_val = $arr[$i];
            $j = $i - 1;

            while (isset($arr[$j]) && $arr[$j] < $cur_val) {
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $cur_val;
                $j--;
            }
        }
        return $arr;
    } else {
        return $arr;
    }

    return '';
}
$array = [1, 10, 2, 4, 6, 3, 56, 21];
print_r(sortArr($array, 'asc'));
echo '<br><br>';

// **3
echo '**3' . ' <br>';

/**
 * 3
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @param integer $number
 * @return string
 */
function searchArrayNumber(array $arr, int $number): string
{
    foreach ($arr as $key => $value) {
        if ($value == $number) {
            return $key . ' - ' . $value;
        }
    }
    return 'ничео не найдено';
}
print_r(searchArrayNumber($array, 10));
echo '<br><br>';

// ***1
echo '***1' . ' <br>';
/**
 * 1
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @return string
 */
function customPrintR(array $arr): string
{
    foreach ($arr as $key => $value) {
        echo $key . '=>' . $value . '<br>';
    }
    return false;
}
echo customPrintR($array);
