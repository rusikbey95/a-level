<?php
session_start();

if (@$_REQUEST['checkoutSubmit']) {
    echo '<pre>';
    print_r($_REQUEST);
    echo '</pre>';
}
?>
<style>
    label{
        display: block;
    }
    input, textarea{
        display: block;
    }
    .item{
        margin-bottom: 10px;
    }
    .item-flex{
        display: flex;
        flex-wrap: wrap;
        align-items: center;
    }
    .item-flex .item{
        margin-left: 10px;
        margin-bottom: 0;
    }
    .item-flex label,
    .item-flex input{
        display: inline-block;
    }
</style>
<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="POST">
    <div class="item">
        <b>Вы выбрали:</b>
        <br>
        <span>
            Название товара: <?php echo $_SESSION['productName']; ?>
            <input type="hidden" name="productName" value="<?php echo $_SESSION['productName']; ?>">
            <br>
            Кол-во: <?php echo $_SESSION['productCount']; ?>
            <input type="hidden" name="productCount" value="<?php echo $_SESSION['productCount']; ?>">
        </span>
    </div>

    <div class="item">
        <label for="checkout_name">Ваше имя</label>
        <input type="text" name="name" id="checkout_name" required>
    </div>
    <div class="item">
        <label for="checkout_phone">Ваш номер</label>
        <input type="tel" name="phone" id="checkout_phone" required>
    </div>
    <div class="item item-flex">
        <b>Тип доставки:</b>
        <div class="item">
            <input type="radio" name="deliveryType" value="courier delivery" id="courier_delivery" required>
            <label for="courier_delivery">Доставка курьером</label>
        </div>
        <div class="item">
            <input type="radio" name="deliveryType" value="ukr post delivery" id="ukr_post_delivery" required>
            <label for="ukr_post_delivery">Доставка Укр почтой</label>
        </div>
        <div class="item">
            <input type="radio" name="deliveryType" value="new post delivery" id="new_post_delivery" required>
            <label for="new_post_delivery">Доставка Новой почтой</label>
        </div>
    </div>
    <div class="item">
        <label for="delivery_address">Адрес доставки</label>
        <input type="text" name="deliveryAddress" id="delivery_address" required>
    </div>
    <div class="item">
        <input type="submit" name="checkoutSubmit" value="Оформить заказ">
    </div>
</form>
