<?php
if (@$_REQUEST['callBackSubmit']) {
    echo '<pre>';
    print_r($_REQUEST);
    echo '</pre>';
}
?>
<style>
    label{
        display: block;
    }
    input, textarea{
        display: block;
    }
    .item{
        margin-bottom: 10px;
    }
</style>
<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="POST">
    <div class="item">
        <label for="call_back_name">Ваше имя</label>
        <input type="text" name="name" id="call_back_name" required>
    </div>
    <div class="item">
        <label for="call_back_phone">Ваш номер</label>
        <input type="tel" name="phone" id="call_back_phone" required>
    </div>
    <div class="item">
        <input type="submit" name="callBackSubmit" value="Перезвонить">
    </div>
</form>
