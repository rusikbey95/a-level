<?php
$valid_types = array('pdf', 'excel', 'word', 'png', 'jpg', 'jpeg');

if (isset($_FILES['userfile'])) {
    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
        $filename = basename($_FILES['userfile']['name']);
        $ext = substr($_FILES['userfile']['name'], 1 + strrpos($_FILES['userfile']['name'], '.'));

        if (!in_array($ext, $valid_types)) {
            echo 'Error: Invalid file type.';
        } else {
            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/a-level/hw4/file/';
            $uploadfile = $uploaddir . $filename;
            move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
            echo 'Success: file loaded';
        }
    } else {
        echo 'Error: empty file.';
    }
} else {
    echo '
        <form enctype="multipart/form-data" method="post">
            Send this file: <input name="userfile" type="file">
            <input type="submit" value="Send File">
        </form>
    ';
}
