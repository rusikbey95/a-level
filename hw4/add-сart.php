<?php
session_start();
$_SESSION['productName'] = $_REQUEST['productName'];
$_SESSION['productCount'] = $_REQUEST['productCount'];

echo $_SESSION['productName'];
echo '<br>';
echo $_SESSION['productCount'];

if (@$_REQUEST['addCartSubmit']) {
    echo '<pre>';
    print_r($_REQUEST);
    echo '</pre>';
}
?>
<style>
    label{
        display: block;
    }
    input, textarea{
        display: block;
    }
    .item{
        margin-bottom: 10px;
    }
</style>
<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="POST">
    <div class="item">
        <label for="product_name">Выберите товар</label>
        <select name="productName" id="product_name">
            <option value="product1">product1</option>
            <option value="product2">product2</option>
            <option value="product3">product3</option>
            <option value="product4">product4</option>
            <option value="product5">product5</option>
        </select>
    </div>
    <div class="item">
        <label for="product_count">Кол-во</label>
        <input type="number" name="productCount" id="product_count" min="1" placeholder="0" required>
    </div>
    <div class="item">
        <input type="submit" name="addCartSubmit" value="Добавить в корзину">
    </div>
</form>
