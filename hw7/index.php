<?php

// *1
echo '*1' . ' <br>';

/**
 * 1 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @param int $c
 * @return int
 */
$checkMinNumber = function (int $a, int $b, int $c): int {
    if ($a < $b && $a < $c) {
        return $a;
    } elseif ($b < $a && $b < $c) {
        return $b;
    } else {
        return $c;
    }
};
echo $checkMinNumber(10, 52, 14);
echo '<br>';

/**
 * 1 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @param int $c
 * @return int
 */
$checkMaxNumber = function (int $a, int $b, int $c): int {
    if ($a > $b && $a > $c) {
        return $a;
    } elseif ($b > $a && $b > $c) {
        return $b;
    } else {
        return $c;
    }
};
echo $checkMaxNumber(10, 52, 14);
echo '<br><br>';

// *2
echo '*2' . ' <br>';

/**
 * 2 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @return int
 */
$calcSquare = function (int $a, int $b): int {
    return $a * $b;
};
echo $calcSquare(10, 10);
echo '<br>';

/**
 * 2 - arrow
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @return int
 */
$calcSquareArrow = fn (int $a, int $b): int => $a * $b;
echo $calcSquareArrow(10, 10);
echo '<br><br>';

// *3
echo '*3' . ' <br>';

/**
 * 3 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @return float
 */
$calcPythagoreanTheorem = function (int $a, int $b): float {
    $c = $a * $a + $b * $b;
    return sqrt($c);
};
echo $calcPythagoreanTheorem(2, 4);
echo '<br>';

/**
 * 3 - arrow
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @return float
 */
$calcPythagoreanTheoremArrow = fn (int $a, int $b) : float => sqrt($a * $a + $b * $b);
echo $calcPythagoreanTheoremArrow(2, 4);
echo '<br><br>';

// *4
echo '*4' . ' <br>';

/**
 * 4 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @return int
 */
$calcPerimeter = function (int $a, int $b): int {
    return 2 * ($a + $b);
};
echo $calcPerimeter(2, 4);
echo '<br>';

/**
 * 4 - arrow
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @return int
 */
$calcPerimeterArrow = fn (int $a, int $b): int => 2 * ($a + $b);
echo $calcPerimeterArrow(2, 4);
echo '<br><br>';

// *5
echo '*5' . ' <br>';

/**
 * 5 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @param int $c
 * @return float
 */
$calcDiscriminant = function (int $a, int $b, int $c): float {
    return $b * $b - 4 * $a * $c;
};
echo $calcDiscriminant(4, 3, 2);
echo '<br>';

/**
 * 5 - arrow
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @param int $b
 * @param int $c
 * @return float
 */
$calcDiscriminantArrow = fn (int $a, int $b, int $c): float => $b * $b - 4 * $a * $c;
echo $calcDiscriminantArrow(4, 3, 2);
echo '<br><br>';

// *6
echo '*6' . ' <br>';

/**
 * 6 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @return array
 */
$getEvenNumbers = function (int $a): array {
    $arr = [];
    for ($i = 1; $i <= $a; $i++) {
        if ($i % 2 == 0) {
            $arr[] = $i;
        }
    }
    return $arr;
};
print_r($getEvenNumbers(100));
echo '<br><br>';

// *7
echo '*7' . ' <br>';

/**
 * 7 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param int $a
 * @return array
 */
$getOddNumbers = function (int $a): array {
    $arr = [];
    for ($i = 1; $i <= $a; $i++) {
        if ($i % 2 !== 0) {
            $arr[] = $i;
        }
    }
    return $arr;
};
print_r($getOddNumbers(100));
echo '<br><br>';

// *8
echo '*8' . ' <br>';
$array = [23, 2, 2, 3, 45, 3, 56];
$arrResult = [];

/**
 * 8 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @return array
 */
$checkRepeatNumbers = function (array $arr): array {
    foreach ($arr as $key => $value) {
        if (isset($arrResult[$value])) {
            $arrResult[$value] += 1;
        } else {
            $arrResult[$value] = 1;
        }
    }
    return $arrResult;
};
print_r($checkRepeatNumbers($array));
echo '<br><br>';

// **9
echo '**9' . ' <br>';

/**
 * 9 - declaration
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @return float
 */
function countArr(array $arr): float
{
    $count = 0;
    foreach ($arr as $value) {
        $count++;
    }
    return $count;
};

/**
 * 9 - expression
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @param string $order
 * @return array
 */
$sortArr = function (array $arr, string $order = 'asc'): array {
    $count = countArr($arr);
    if ($order == 'asc') {
        for ($i = 1; $i < $count; $i++) {
            $cur_val = $arr[$i];
            $j = $i - 1;

            while (isset($arr[$j]) && $arr[$j] > $cur_val) {
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $cur_val;
                $j--;
            }
        }
        return $arr;
    } elseif ($order == 'desc') {
        for ($i = 1; $i < $count; $i++) {
            $cur_val = $arr[$i];
            $j = $i - 1;

            while (isset($arr[$j]) && $arr[$j] < $cur_val) {
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $cur_val;
                $j--;
            }
        }
        return $arr;
    } else {
        return $arr;
    }
};
$array = [1, 10, 2, 4, 6, 3, 56, 21];
print_r($sortArr($array, 'asc'));
echo '<br><br>';
