<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>hello</h1>
    <script>
        /*
            Task - 1
            Если в переменную вписана строка , то выводим в консоль сообщение "Привет, ЗНАЧНИЕ_NAME"
            Если в переменную вписана не строка, а любой другой тип данных, то выводим сообщение "Ошибка, не тот тип данных"
        */
        let name = 'Ivan';
        if (typeof name === 'string') {
            console.log('Привет, ' + name);
        } else {
            console.log('Ошибка, не тот тип данных');
        }

        /*
            Task - 2
            Вывести в консоль примеры всех типов данных
        */
        console.log(typeof undefined); //undefined
        console.log(typeof 123); //number
        console.log(typeof 123n); //bigint
        console.log(typeof 'Hello'); //string
        console.log(typeof true); //boolean
        console.log(typeof Symbol('id')); //Symbol
        console.log(typeof null); //null - object
        console.log(typeof {}); //object

    </script>
</body>
</html>