<?php

/**
 * counting the length of the array
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param array $arr
 * @return string
 */
function countArr(array $arr): string
{
    $count = 0;
    foreach ($arr as $key => $value) {
        $count++;
    }
    return $count;
}

/**
 * counting the length of the string
 *
 * @author Ruslan Bey <rusikbey95@gmail.com>
 *
 * @param string $string
 * @return string
 */
function countStr($string): string
{
    $lengthStr1 = 0;
    $lengthStr2 = 0;
    for (; $string[$lengthStr1] != null; $lengthStr1++) {
        $lengthStr2++;
    }
    return $lengthStr2;
}
