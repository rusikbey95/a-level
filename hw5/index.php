<?php
include $_SERVER['DOCUMENT_ROOT'] . '/a-level/hw5/function.php';

// **1
echo '<b>**1</b><br>';

for ($x = 0; $x <= 100; $x++) {
    $x1 = $x % 2;
    $x2 = $x % 3;
    $x3 = $x % 5;

    if ($x > 1 && (($x1 >= 1) && ($x2 >= 1) && ($x3 >= 1)) || in_array($x, [2, 3, 5])) {
        echo $x . '<br>';
    }
}
echo '<br>';

// **2
echo '<b>**2</b><br>';

$y = 0;
$arr = [];
while ($y <= 100) {
    $y++;
    if ($y % 2 == 0) {
        $arr[] = $y;
    }
}
$countArr = 0;
while ($arr[$countArr] != null) {
    $countArr++;
}
echo $countArr;
echo '<br><br>';

// **3
echo '<b>**3</b><br>';

$generatedHundredNumber = 0;
$arrRandom = [];
while ($generatedHundredNumber < 100) {
    $generatedHundredNumber++;
    $arrRandom[] = rand(1, 5);
}
$item1 = [];
$item2 = [];
$item3 = [];
$item4 = [];
$item5 = [];
foreach ($arrRandom as $item) {
    switch ($item) {
        case 1:
            $item1[] = $item;
            break;
        case 2:
            $item2[] = $item;
            break;
        case 3:
            $item3[] = $item;
            break;
        case 4:
            $item4[] = $item;
            break;
        case 5:
            $item5[] = $item;
            break;
    }
}
echo count($item1) . '<br>' .
     count($item2) . '<br>' .
     count($item3) . '<br>' .
     count($item4) . '<br>' .
     count($item5);
echo '<br><br>';

// **4

echo '<b>**4</b><br>';
$tableRow = 0;
$tableLine = 0;
echo '<table border="1">';
for ($tableLine = 0; $tableLine < 3; $tableLine++) {
    echo '<tr>';
    for ($tableRow = 0; $tableRow < 5; $tableRow++) {
        echo '<td style=background-color:rgb(' . rand(0, 255). ',' . rand(0, 255). ',' . rand(0, 255) . ');>' . $tableRow . '</td>';
    }
}
echo '</tr>';
echo '</table>';
echo '<br><br>';

// **5
echo '<b>**5</b><br>';
$month = rand(1, 12);
//echo $month;
switch (true) {
    case $month >= 3 && $month <= 5:
        echo "it's spring";
        break;
    case $month >= 6 && $month <= 8:
        echo "it's summer";
        break;
    case $month >= 9 && $month <= 11:
        echo "it's autumn";
        break;
    default:
        echo "it's winter";
}
echo '<br>';
if ($month >= 3 && $month <= 5) {
    echo "it's spring";
} elseif ($month >= 6 && $month <= 8) {
    echo "it's summer";
} elseif ($month >= 9 && $month <= 11) {
    echo "it's autumn";
} else {
    echo "it's winter";
}
echo '<br><br>';

// **6
echo '<b>**6</b><br>';
$str = 'abcde';
if ($str[0] == 'a') {
    echo 'да';
} else {
    echo 'нет';
}
echo '<br><br>';

// **7
echo '<b>**7</b><br>';
$number = '12345';
if ($number[0] == 1 || $number[0] == 2 || $number[0] == 3) {
    echo 'да';
} else {
    echo 'нет';
}
echo '<br><br>';

// **8
echo '<b>**8</b><br>';
$test = true;
echo $test ? 'Верно' : 'Неверно';
echo '<br>';
if ($test) {
    echo 'Верно';
} else {
    echo 'Неверно';
}
echo '<br><br>';

// **9
echo '<b>**9</b><br>';
$lang = 'eng';
$ru = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$eng = ['mn', 'ts', 'ws', 'ts', 'fr', 'st', 'sn'];

echo $lang == 'ru' ? print_r($ru) : print_r($eng);
echo '<br>';
if ($lang == 'ru') {
    print_r($ru);
} else {
    print_r($eng);
}
echo '<br><br>';

// **10
echo '<b>**10</b><br>';
$cloсk = rand(0, 59);
echo $cloсk . '<br>';
if ($cloсk <= 15) {
    echo 'первая четверть';
} else if ($cloсk > 15 && $cloсk <= 30) {
    echo 'вторая четверть';
} else if ($cloсk > 30 && $cloсk <= 45) {
    echo 'третья четверть';
} else {
    echo 'четвертая четверть';
}
echo '<br>';
echo $cloсk <= 15 ? 'первая четверть' : '';
echo $cloсk > 15 && $cloсk <= 30 ? 'вторая четверть' : '';
echo $cloсk > 30 && $cloсk <= 45 ? 'третья четверть' : '';
echo $cloсk > 45 && $cloсk <= 59 ? 'четвертая четверть' : '';
echo '<br><br>';

// ***1
echo '<b>***1</b><br>';
$arrCount = [1, 2, 3, '123', 123];
$count = 0;
while ($arrCount[$count]) {
    $count++;
}

do {
    $count++;
} while ($arrCount[$count]);

for (; $arrCount[$count]; $count++);

foreach ($arrCount as $key => $value) {
    $count++;
}
echo $count;
echo '<br><br>';

// ***2
echo '<b>***2</b><br>';
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];

$arrReverse = [];
$i = countArr($arr) - 1;
while ($i >= 0) {
    $arrReverse[] = $arr[$i];
    $i--;
}

do {
    $arrReverse[] = $arr[$i];
} while (--$i >= 0);

for ($i = countArr($arr) - 1; $i >= 0; $i--) {
    $arrReverse[] = $arr[$i];
}

$count = countArr($arr);
foreach ($arr as $key => $item) {
    $count--;
    $arrReverse[] = $arr[$count];
}
print_r($arrReverse);
echo '<br><br>';

// ***3
echo '<b>***3</b><br>';
$numbers = [44, 12, 11, 7, 1, 99, 43, 5, 69];

$arrReverse2 = [];
$i = countArr($numbers) - 1;
while ($i >= 0) {
    $arrReverse2[] = $numbers[$i];
    $i--;
}

do {
    $arrReverse2[] = $numbers[$i];
} while (--$i >= 0);

for ($i = countArr($numbers) - 1; $i >= 0; $i--) {
    $arrReverse2[] = $numbers[$i];
}

$count = countArr($numbers);
foreach ($numbers as $key => $item) {
    $count--;
    $arrReverse2[] = $numbers[$count];
}
print_r($arrReverse2);
echo '<br><br>';

// ***4
echo '<b>***4</b><br>';
$str = 'Hi I am ALex';

$i = countStr($str);

while ($i > 0) {
    echo $str[$i - 1];
    $i--;
}

do {
    echo $str[$i - 1];
} while (--$i > 0);

for ($i = countStr($str); $i > 0; $i--) {
    echo $str[$i - 1];
}

$arr3 = [];
for ($i = countStr($str); $i > 0; $i--) {
    $arr3[] = $str[$i - 1];
}
foreach ($arr3 as $key => $value) {
    echo $str[$i - 1];
    $i--;
}
echo '<br><br>';

// ***5
echo '<b>***5</b><br>';
$str5 = 'Hi I am ALex';
$lowerLetters = [
    'A' => 'a',
    'B' => 'b',
    'C' => 'c',
    'D' => 'd',
    'E' => 'e',
    'F' => 'f',
    'G' => 'g',
    'H' => 'h',
    'I' => 'i',
    'J' => 'j',
    'K' => 'k',
    'L' => 'l',
    'M' => 'm',
    'N' => 'n',
    'O' => 'o',
    'P' => 'p',
    'Q' => 'q',
    'R' => 'r',
    'S' => 's',
    'T' => 't',
    'U' => 'u',
    'V' => 'v',
    'W' => 'w',
    'X' => 'x',
    'Y' => 'y',
    'Z' => 'z',
];

$str5Length = countStr($str5);
$i5 = 0;

while ($i5 <= $str5Length) {
    foreach ($lowerLetters as $key => $val) {
        if ($str5[$i5] == $key) {
            $str5[$i5] = $val;
        }
    }
    $i5++;
}

do {
    foreach ($lowerLetters as $key => $val) {
        if ($str5[$i5] == $key) {
            $str5[$i5] = $val;
        }
    }
} while ($i5++ <= $str5Length);

for (; $i5 <= $str5Length; $i5++) {
    foreach ($lowerLetters as $key => $val) {
        if ($str5[$i5] == $key) {
            $str5[$i5] = $val;
        }
    }
}

$arr5 = [];
for (; $i5 <= $str5Length; $i5++) {
    $arr5[] = $str5[$i5];
}
foreach ($arr5 as $key => $value) {
    foreach ($lowerLetters as $key2 => $val2) {
        if ($str5[$key] == $key2) {
            $str5[$key] = $val2;
        }
    }
}
echo $str5;
echo '<br><br>';

// ***6
echo '<b>***6</b><br>';
$str6 = 'Hi I am ALex';
$upperLetters = [
    'a' => 'A',
    'b' => 'B',
    'c' => 'C',
    'd' => 'D',
    'e' => 'E',
    'f' => 'F',
    'g' => 'G',
    'h' => 'H',
    'i' => 'I',
    'j' => 'J',
    'k' => 'K',
    'l' => 'L',
    'm' => 'M',
    'n' => 'N',
    'o' => 'O',
    'p' => 'P',
    'q' => 'Q',
    'r' => 'R',
    's' => 'S',
    't' => 'T',
    'u' => 'U',
    'v' => 'V',
    'w' => 'W',
    'x' => 'X',
    'y' => 'Y',
    'z' => 'Z',
];

$str6Length = countStr($str6);
$i6 = 0;

while ($i6 <= $str6Length) {
    foreach ($upperLetters as $key => $val) {
        if ($str6[$i6] == $key) {
            $str6[$i6] = $val;
        }
    }
    $i6++;
}

do {
    foreach ($upperLetters as $key => $val) {
        if ($str6[$i6] == $key) {
            $str6[$i6] = $val;
        }
    }
} while ($i6++ <= $str6Length);

for (; $i6 <= $str6Length; $i6++) {
    foreach ($upperLetters as $key => $val) {
        if ($str6[$i6] == $key) {
            $str6[$i6] = $val;
        }
    }
}

$arr6 = [];
for (; $i6 <= $str6Length; $i6++) {
    $arr6[] = $str6[$i6];
}
foreach ($arr6 as $key => $value) {
    foreach ($upperLetters as $key2 => $val2) {
        if ($str6[$key] == $key2) {
            $str6[$key] = $val2;
        }
    }
}
echo $str6;
echo '<br><br>';

// ***7
echo '<b>***7</b><br>';
$arr7 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arr7Length = countArr($arr7);

$i7 = 0;
while ($i7 < $arr7Length) {
    $arr7[$i7] = strtolower($arr7[$i7]);
    $i7++;
}

do {
    $arr7[$i7] = strtolower($arr7[$i7]);
} while (++$i7 < $arr7Length);

for ($i7 = 0; $i7 < $arr7Length; $i7++) {
    $arr7[$i7] = strtolower($arr7[$i7]);
}

foreach ($arr7 as &$value) {
    $value = strtolower($value);
}
echo '<br><br>';

// ***8
echo '<b>***8</b><br>';
$arr8 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arr8Length = countArr($arr8);

$i8 = 0;
while ($i8 < $arr8Length) {
    $arr8[$i8] = strtoupper($arr8[$i8]);
    $i8++;
}

do {
    $arr8[$i8] = strtoupper($arr8[$i8]);
} while (++$i8 < $arr8Length);

for ($i8 = 0; $i8 < $arr8Length; $i8++) {
    $arr8[$i8] = strtoupper($arr8[$i8]);
}

foreach ($arr8 as &$value) {
    $value = strtoupper($value);
}

echo '<br><br>';

// ***9
echo '<b>***9</b><br>';
$num9 = '12345678';
$num9Count = countStr($num9);

while ($num9Count >= 0) {
    echo $num9{$num9Count};
    $num9Count--;
}

do {
    echo $num9{$num9Count};
} while (--$num9Count >= 0);

for (; $num9Count >= 0; $num9Count--) {
    echo $num9{$num9Count};
}

$arr9 = [];
for ($str = 1; $str <= $num9Count; $str++) {
    $arr9[] = $num9[$str];
}
foreach ($arr9 as $key => $value) {
    echo $num9Count;
    --$num9Count;
}
echo '<br><br>';

// ***10
echo '<b>***10</b><br>';
$arr10 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$i = 0;
$i10 = 0;
$k10 = 0;
$newArr10 = [];

while ($arr10[$i]) {
    $i2 = 0;
    while ($arr10[$i2]) {
        $i2++;
        if ($arr10[$i] > $i10) {
            $i10 = $arr10[$i2];
            $k10 = $arr10[$i];
        }
    }
    $i++;
    $newArr10[] = $i10;
    $arr10[$k10] = 0;
    $i10 = 0;
    $k10 = 0;
}

do {
    $newArr10[] = $i10;
    $arr10[$k10] = 0;
    $i10 = 0;
    $k10 = 0;
    do {
        if ($arr10[$i] > $i10) {
            $i10 = $arr10[$i2];
            $k10 = $arr10[$i];
        }
        $i2++;
    } while ($arr10[$i2]);
    $i++;
} while ($arr10[$i]);

foreach ($arr10 as $index => $item) {
    foreach ($arr10 as $key => $value) {
        if ($value > $i10) {
            $i10 = $value;
            $k10 = $key;
        }
    }
    $newArr10[] = $i10;
    $arr10[$k10] = 0;
    $i10 = 0;
    $k10 = 0;
}
