<?php

//@author Ruslan Bey <rusikbey95@gmail.com>

//*
echo '<b>* 1</b><br>';
    echo 42 > 55 ? 'More' : 'Less';
    echo '<br>';

echo '<b>* 2</b><br>';
    echo rand(5, 15) > rand(5, 15) ? 'More' : 'Less';
    echo '<br>';

echo '<b>* 3</b><br>';
    $firstName = 'Ruslan';
    $lastName = 'Bey';
    $patronymic = 'Ruslanovich';
    echo $lastName . ' ' . $firstName[0] . '. ' . $patronymic[0] . '.';
    echo '<br>';

echo '<b>* 4</b><br>';
    $number = 123456789123456789;
    $numberRand = rand(1, 99999);
    echo substr_count($number, $numberRand);
    echo '<br>';

echo '<b>* 5</b><br>';
    echo '<b>a</b><br>';
        $a = 3;
        echo $a;
        echo '<br>';

    echo '<b>b</b><br>';
        $a = 10;
        $b = 2;
        echo $a + $b;
        echo '<br>';
        echo $a - $b;
        echo '<br>';
        echo $a * $b;
        echo '<br>';
        echo $a / $b;
        echo '<br>';

    echo '<b>c</b><br>';
        $c = 15;
        $d = 2;
        echo $result = $c + $d;
        echo '<br>';

    echo '<b>d</b><br>';
        $a = 10;
        $b = 2;
        $c = 5;
        echo $a + $b + $c;
        echo '<br>';

    echo '<b>e</b><br>';
        $a = 17;
        $b = 10;
        $c = $a - $b;
        echo $d = $c;
        echo '<br>';

echo '<b>* 6</b><br>';
    echo $result = $c + $d;
    echo '<br>';

echo '<b>* 7</b><br>';
    echo '<b>a</b><br>';
        echo $text = 'Привет, Мир!';
        echo '<br>';

    echo '<b>b</b><br>';
        $text1 = 'Привет, ';
        $text2 = 'Мир!';
        echo $text1 . $text2;
        echo '<br>';

    echo '<b>с</b><br>';
        $seconds = 3600;
        $hours = 1;
        $day = 24;
        $week = 24 * 7;
        $monthDays = 24 * 30;
        echo $seconds * $hours;
        echo '<br>';
        echo $seconds * $day;
        echo '<br>';
        echo $seconds * $week;
        echo '<br>';
        echo $seconds * $monthDays;
        echo '<br>';

echo '<b>* 8</b><br>';
    $var = 1;
    $var += 12;
    $var -= 14;
    $var *= 5;
    $var /= 7;
    $var %= 1;
    echo $var;
    echo '<br>';

//**
echo '<b>** 1</b><br>';
    date_default_timezone_set('Europe/Kiev');
    $second = date('s');
    $minute = date('i');
    $hour = date('G');
    echo $hour . ':' . $minute . ':' . $second;
    echo '<br>';

echo '<b>** 2</b><br>';
    $text = 'Я';
    $text .= ' хочу';
    $text .= ' знать';
    $text .= ' PHP!';
    echo $text;
    echo '<br>';

echo '<b>** 3</b><br>';
    $foo = 'bar';
    $bar = 10;
    echo $$foo;
    echo '<br>';

echo '<b>** 4</b><br>';
    echo 6;
    echo '<br>';
    echo 8;
    echo '<br>';
    echo 9;
    echo '<br>';

echo '<b>** 5</b><br>';
    echo '<b>a</b><br>';
    $a = [];
    echo isset($a) ? 'Переменная существует' : 'Перенменная не существует';
    echo '<br>';

    echo '<b>b</b><br>';
    echo gettype($a) ? 'Переменная boolean' : 'Переменная не boolean';
    echo '<br>';

    echo '<b>с</b><br>';
    echo is_null($a) ? 'Переменная null' : 'Переменная не null';
    echo '<br>';

    echo '<b>d</b><br>';
    echo empty($a) ? 'Переменная пуста' : 'Переменная не пуста';
    echo '<br>';

    echo '<b>e</b><br>';
    echo is_integer($a) ? 'Переменная число' : 'Переменная не число';
    echo '<br>';

    echo '<b>f</b><br>';
    echo is_double($a) ? 'Переменная с плавающей точкой' : 'Переменная без с плавающей точки';
    echo '<br>';

    echo '<b>g</b><br>';
    echo is_string($a) ? 'Переменная строка' : 'Переменная не строка';
    echo '<br>';

    echo '<b>h</b><br>';
    echo is_numeric($a) ? 'Переменная число или строка с числом' : 'Переменная не число и не строка с числом';
    echo '<br>';

    echo '<b>i</b><br>';
    echo is_bool($a) ? 'Переменная boolean' : 'Переменная не boolean';
    echo '<br>';

    echo '<b>j</b><br>';
    echo is_scalar($a) ? 'Переменная скалярная' : 'Переменная не скалярная';
    echo '<br>';

    echo '<b>k</b><br>';
    echo is_null($a) ? 'Переменная null' : 'Переменная не null';
    echo '<br>';

    echo '<b>l</b><br>';
    echo is_array($a) ? 'Переменная массив' : 'Переменная не массив';
    echo '<br>';

    echo '<b>m</b><br>';
    echo is_object($a) ? 'Переменная объект' : 'Переменная не объект';
    echo '<br>';

//***
echo '<b>*** 1</b><br>';
    $a = 10;
    $b = 2;
    echo $a + $b . ' ' .  $a * $b;
    echo '<br>';

echo '<b>*** 2</b><br>';
    $c = 2;
    $d = 6;
    echo pow($c, 2) + pow($d, 2);
    echo '<br>';

echo '<b>*** 3</b><br>';
    $a = 2;
    $b = 4;
    $c = 8;
    echo ($a + $b + $c) / 3;
    echo '<br>';

echo '<b>*** 4</b><br>';
    $x = 10;
    $y = 15;
    $z = 20;
    echo ($x + 1) - 2 * ($z - 2 * $x + $y);
    echo '<br>';

echo '<b>*** 5</b><br>';
    $a = 21;
    echo $a % 3 . ' ' . $a % 5;
    echo '<br>';
    echo $a * 30 / 100 + $a;
    echo '<br>';
    echo $a * 120 / 100 + $a;
    echo '<br>';

echo '<b>*** 6</b><br>';
    $a = 20;
    $b = 60;
    echo ($a * 40 / 100) + ($b * 84 / 100);
    echo '<br>';
    $c = 112;
    $c .= $c;
    echo $c[0] + $c[1] + $c[2];
    echo '<br>';

echo '<b>*** 7</b><br>';
    $a = 123;
    $a = (string)$a;
    $a[1] = 0;
    echo $a[2] . $a[1] . $a[0];
    echo '<br>';

echo '<b>*** 8</b><br>';
    $random = rand(1, 200);
    echo $random;
    echo '<br>';
    echo $random % 2 == 0 ? 'Число четное' : 'Число не четное';
    echo '<br>';
