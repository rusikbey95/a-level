<?php
    $myName = 'Ruslan';
    $myAge = 26;
    $pi = 3.14;
    $names = ['alex', 'vova', 'tolya'];
    $names2 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
    $names3 = ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];
    $names4 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>hw2</title>
</head>
<body>
    <ul>
        <li>My name: <?php echo $myName ?></li>
        <li>My age: <?php echo $myAge ?></li>
        <li>Pi: <?php echo $pi ?></li>
        <li>
            <pre><?php print_r($names); ?></pre>
        </li>
        <li>
            <pre><?php print_r($names2); ?></pre>
        </li>
        <li>
            <pre><?php print_r($names3); ?></pre>
        </li>
            <pre><?php print_r($names4); ?></pre>
        </li>
    </ul>
</body>
</html>
