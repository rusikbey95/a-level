<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Second js lesson</h1>

<script>
    /*
    Task - 1
    Есть массив ['Капуста', 'Репа', 'Редиска', 'Морковка']. Надо вывести в консоль строку 'Капуста | Репа | Редиска | Морковка';
    */
    let vegetables = ['Капуста', 'Репа', 'Редиска', 'Морковка'];
    console.log(vegetables.join(' | '))

    /*
    Task - 2
    Есть строка 'Вася;Петя;Вова;Олег'. Используя стандартные методы строк получить массив их имен;
    */
    let names = 'Вася;Петя;Вова;Олег';
    const newArr = names.split(';');
    console.log(newArr);

    /*
    Task - 3
    Напишите функцию hello2(), которая при вызове будет принимать переменную (в аргументы) name (например, «Василий»)
    и выводить строку (в нашем случае «Привет, Василий»).
    В случае отсутствующего аргумента выводить «Привет, гость»
    */
    let name = 'Василий';
    function hello2(name) {
        if (name) {
            return 'Привет, ' + name;
        }
        return 'Привет, гость';
    }
    console.log(hello2(name));

    /*
    Task - 4
    Есть массив ['яблоко', 'ананас', 'груша']
    Привести каждый элемент массива в верхний регистр (сделать все слово большими буквами) и получить результат (новый массив) в новую переменную.
    */
    const fruits = ['яблоко', 'ананас', 'груша'];
    const fruitsInUpperCase = fruits.map(
        function(arr) {
            return arr.toUpperCase();
        }
    );
    console.log(fruitsInUpperCase);

    /*
    Task - 5
    Написать функцию addOneForAll, которая может принять неограниченное кол-во аргументов.
    Добавить к каждому аргументу 1 и вернуть новый массив с новыми значениями.

    Пример:
    передал в массив такие числа - 1, 2, 3, 4
    функция добавляет к каждму числу + 1
    функция возвращает новый массив, в котором новые значения
    */
    function addOneForAll(...arrNumber) {
        // let toArr = String(arrNumber).split(', ')
        let arr = arrNumber.map(function(num) {
            return num + 1;
        });
        return arr;
    }
    const val = addOneForAll(1, 2, 3, 4);
    console.log(val);

    /*
    Task - 6
    Написать функцию getSum, которая может принять неограниченное кол-во аргументов и возвращает их сумму.
    const val = getSum(1, 2, 3, 4);
    console.log(val); // 10
    */
    function getSum(...args) {
        let sum = args.reduce(function (previousValue, currentValue, index, array) {
            return previousValue + currentValue;
        });
        return sum;
    }
    const val2 = getSum(1, 2, 3, 4);
    console.log(val2);

    /*
    Task - 7
    Есть массив [1, 'hello', 2, 3, 4, '5', '6', 7, null].
    Отфильтровать массив так, чтобы остались только числа. Сделать можно любым способом из того, что учили.
    */
    let arr7 = [1, 'hello', 2, 3, 4, '5', '6', 7, null];
    let numberArray = arr7.filter(element => Number.isInteger(element));
    console.log(numberArray);

    /*
    Task - 8
    Написать функцию arrayTesting, которая принимает в себя любой массив (в аргументы)
    функция проверяет есть ли в массиве хоть одно true значение
    и если оно есть, то возвращаем из функции строку 'Нашли true значение', если его нет - 'Ничего нет'
    */
    function arrayTesting(arr) {
        let isTrue = arr.filter(element => Boolean(element));
        if (isTrue.length > 0) {
            return 'Нашли true значение';
        }
        return 'Ничего нет';

    }
    const haveTrueValue = arrayTesting([0, false, null, 1]); // Нашли true значение (потому что есть хотябы одно true значение - 1)
    const dontHaveTrueValue = arrayTesting([0, false, null, 0]); // Ничего нет
    console.log(haveTrueValue);
</script>
</body>
</html>